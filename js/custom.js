
const courses = [
    {
        "id": 1,
        "name": "Data Science",
        "description": "Python, R, C++"
    },
    {
        "id": 2,
        "name": "Front-end",
        "description": "Js, ReactJS, React Native"
    },
    {
        "id": 3,
        "name": "Back-end",
        "description": "Java, NodeJs, Php"
    },
    {
        "id": 4,
        "name": "Network Security",
        "description": "Anything"
    },
    {
        "id": 5,
        "name": "Korean",
        "description": "Anything"
    },
]

// Show courses
const showCourses = () => {
    const listCoursesBlock =  document.querySelector('#list-courses').querySelector('tbody');
    const htmls = courses.map( (course) => {
        const {id,name,description} = course; // destructuring
        return `
            <tr class="course-item-${course.id}">
                <td>${id}</td>
                <td>${name}</td>
                <td>${description}</td>
                <td class="text-center" onclick="handleUpdateCourse(${id})"><i class="fa fa-edit fa-lg text-info"></i></td>
                <td class="text-center" onclick="handleDeleteCourse(${id})"><i class="fa fa-trash fa-lg text-info"></i><td>
            </tr>
        `;
    });
    listCoursesBlock.innerHTML = htmls.join('');

}

// Add course
const addCourse = () => {
   
    const createBtn = document.querySelector('#create');
    createBtn.onclick = (e) => {
        e.preventDefault();
        idEl = document.querySelector('input[name="id"]');
        nameEL = document.querySelector('input[name="name"]');
        descriptionEl = document.querySelector('input[name="description"]');
        
        const newCourse = {
            id: idEl.value,
            name: nameEL.value,
            description: descriptionEl.value
        };
        if(newCourse) {
            courses.push(newCourse);

            idEl.value = '';
            nameEL.value = '';
            descriptionEl.value = '';

            showCourses();
        }
    }
    
}

// Update course

const handleUpdateCourse = item => {
    const createBtn = document.querySelector('#create');
    createBtn.innerText = 'Update';

    idEl = document.querySelector('input[name="id"]');
    nameEl = document.querySelector('input[name="name"]');
    descriptionEl = document.querySelector('input[name="description"]');

    // Set value for form update
    idEl.value = courses[item-1].id;
    nameEl.value = courses[item-1].name;
    descriptionEl.value = courses[item-1].description;

    createBtn.onclick = function(e) {
        e.preventDefault();
        
        const course = {
            id: idEl.value,
            name: nameEl.value,
            description: descriptionEl.value
        };
        if(course) {
            courses.splice(item-1, 1, course);
            // clear input value
            idEl.value = '';
            nameEl.value = '';
            descriptionEl.value = '';

            createBtn.innerText = 'Create';
            showCourses();
        }
    }
}

// Delete course
const handleDeleteCourse = item => {
    const course = document.querySelector('.course-item-' + item);
    course.remove();
}

// Search for course
const searchCourse = () => {
    let filter, tr;
    filter = document.getElementById("search").value.toUpperCase();
    tr = document.getElementById("list-courses").querySelector("tbody").getElementsByTagName("tr");

    for (var i = 0; i < tr.length; i++) {
        let td = tr[i].getElementsByTagName("td")[1];
        if (td) {
            let txtValue;
            txtValue = td.textContent || td.innerText;
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }       
    }
}

const start = () => {
    showCourses();
    addCourse();
}
start();
