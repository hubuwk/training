
function Validator(options) {
    function getParent(element, selector) {
        while (element.parentElement) {
            if(element.parentElement.matches(selector)) {
                return element.parentElement;
            }
            element = element.parentElement;
        }
    }

    var selectorRules = {};

    function validate(inputEle, rule) {
        var errEle = getParent(inputEle,options.formGroupSelector).querySelector(options.errSelector);
        // var errEle = inputEle.parentElement.querySelector(options.errSelector);
        var errMessage;

        var rules = selectorRules[rule.selector];

        for (var i = 0; i < rules.length; i++) {
            errMessage = rules[i](inputEle.value);
            if (errMessage) break;
        }
        if (errMessage) {
            
            errEle.innerText = errMessage;
            getParent(inputEle,options.formGroupSelector).classList.add('invalid');
        } else {
            errEle.innerText = '';
            getParent(inputEle,options.formGroupSelector).classList.remove('invalid');
        }
        return !errMessage;
    }

    var formEle = document.querySelector(options.form);

    if (formEle) {
        formEle.onsubmit = function (e) {
            e.preventDefault();

            var isFormValid = true;

            options.rules.forEach(function (rule) {
                var inputEle = formEle.querySelector(rule.selector);
                var isValid = validate(inputEle, rule);
                if(!isValid) {
                    isFormValid = false;
                }
            });
            if (isFormValid) {
                if (typeof options.onSubmit === 'function') {
                    var enableInputs = formEle.querySelectorAll('[name]:not([disabled])');
                    
                    var formValues = Array.from(enableInputs).reduce(function(values,input) {
                        values[input.name] = input.value;
                        return values;
                    }, {});

                    options.onSubmit(formValues);

                } else {
                    // formElement.submit();
                    location.replace("http://127.0.0.1:5500/admin.html");

                }
            }
        }
        options.rules.forEach(function (rule) {
            if (Array.isArray(selectorRules[rule.selector])) {
                selectorRules[rule.selector].push(rule.test);
            } else {
                selectorRules[rule.selector] = [rule.test];
            }

            var inputEle = formEle.querySelector(rule.selector);

            if (inputEle) {
                inputEle.onblur = function () {
                    validate(inputEle, rule);
                }

                inputEle.oninput = function () {
                    var parentElement = getParent(inputEle,options.formGroupSelector);
                    var errEle = parentElement.querySelector(options.errSelector);
                    errEle.innerText = '';
                    parentElement.classList.remove('invalid');
                }
            }

        });
    }
}

Validator.isRequired = function (selector) {
    return {
        selector: selector,
        test: function (value) {
            return value.trim() ? undefined : 'Please fill out the field.';
        }
    }

}

Validator.isEmail = function (selector) {
    return {
        selector: selector,
        test: function (value) {
            var regex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
            return regex.test(value) ? undefined : 'Invalid email';
        }
    }
}

Validator.isPassword = function (selector, min) {
    return {
        selector: selector,
        test: function (value) {
            return value.length >= min ? undefined : `Enter at least ${min} characters`
        }
    }
}

Validator.isCorrectEmail = function (selector, getConfirmValue) {
    return {
        selector: selector,
        test: function (value) {
            return value === getConfirmValue() ? undefined : 'This email does not exist';
        }
    }
}

Validator.isCorrectPassword = function (selector, getConfirmValue) {
    return {
        selector: selector,
        test: function (value) {
            return value === getConfirmValue() ? undefined : 'Wrong password';
        }
    }
}

